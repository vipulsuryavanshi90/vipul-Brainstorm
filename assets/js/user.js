var App = angular.module("user",[]);
App.controller("loginController",["$scope","$http",function($scope,$http){

	$scope.data = {};

	$scope.registerUser = function()
	{
		
		var dict={
			url: 'api/user_registration.php',
			method: 'POST',
	 		data: $scope.data,
 			}

		//console.log(dict);
		$http(dict).success(function(response){
			//console.log(response.message);
			if(response.message == 'User Registered Successfully'){
				window.location.href = 'login.html';
			}
			if(response.message == 'Username Already Exist'){
				alert(response.message);
				$scope.data = {};
			}
			
		});
				
	}

	$scope.loginUser = function(){
		
		var dict = {
			url:"api/user_login.php",
			method:'POST',
			data: $scope.data,
		}

		//console.log($scope.data);
		$http(dict).success(function(response){
			console.log(response);
			if(response.message == 'Successfull Login'){
				window.location.href = 'welcome.html';
			}
			if(response.message == 'Login Failed'){
				alert('Wrong Username or Password Please Enter Correct Input');
				$scope.data = {};
			}
			
		});
	}
}
]);

